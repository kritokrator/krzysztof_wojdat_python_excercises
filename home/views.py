from django.shortcuts import render
from django.shortcuts import render_to_response
from django.shortcuts import get_object_or_404
from django.template import RequestContext
from django.template.defaultfilters import slugify
from home.models import Home,Category
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
import json
#from django.core.context_processors import csrf

#create your views here

def index(request):
	return render(request,'home/index.html')

def blog(request):
	if request.user.is_authenticated:
		return render(request,'home/blog.html',{'categories' : Category.objects.all(),'posts' : Home.objects.filter(owner = request.user.id)[:5]})
	else:
		return render(request,'home/blog.html',{'categories' : Category.objects.all(),'posts' : Home.objects.all()[:5]})

def view_post(request,slug):
	return render(request,'home/post.html',{
		'post':get_object_or_404(Home,slug=slug)
		})

def view_category(request,slug):
	category = get_object_or_404(Category, slug = slug)
	return render('home/blog.html',{
		'category' : category,
		'posts' : Home.objects.filter(category=category)[:5],
		'categories': Category.objects.all()
		})
def view_users(request):
	users = User.objects.all()
	#context_instance = RequestContext(request)
	#context_instance = {'users' : users }
	#context_instance.update(csrf(request))
	#context_instance.push({'users' : users})
	#return render_to_response('home/users.html',context=context_instance)
	return render(request,'home/users.html',{'users':users})

def login_user(request):
	user_login = request.POST['username']
	user_pass = request.POST['password']
	user = authenticate(username=user_login,password = user_pass)
	if user is not None:
		login(request, user)
		return render(request,'home/blog.html',{'categories' : Category.objects.all(),
				'posts' : Home.objects.filter(owner=user.id)[:5],
				'login_message' : 'user logged in',
				'user' : request.user})
	else:
		return render(request,'home/blog.html',{'login_message' : 'login unsuccessful'})

def logout_user(request):
	logout(request)
	return render(request,'home/blog.html',{'login_message' : 'user logged out'})

def login_page(request):
	return render(request,'home/login.html')	

def create_user(request):
	#context_instance = RequestContext(context)
	username = request.POST['username']
	first_name = request.POST['first_name']
	last_name = request.POST['last_name']
	email = request.POST['email']
	password = request.POST['password']
		
	user = User.objects.create_user(username,email,password)
	user.last_name = last_name
	user.first_name = first_name
	user.save()
	users = User.objects.all()
	#context_instance = {'users' : users}
	#context_instance.update(csrf(request))
	#context_instance.push({'users' : users})
	#return render_to_response('home/users.html', context = context_instance)
	return render(request,'home/users.html',{'users' : users })
def add_home(request):
	return render(request,'home/add_home.html', {'categories' : Category.objects.all()})

def add_post(request):
	new_title = request.POST['title']
	new_slug = slugify(new_title)
	new_body = request.POST['body']
	new_owner = request.user.id
	new_category = Category.objects.get(title = request.POST['category'])
	home = Home()
	home.title = new_title
	home.slug = new_slug
	home.body = new_body
	home.owner= new_owner
	home.category = new_category
	home.position = 0
	home.save()
	return render(request,'home/blog.html',{'categories' : Category.objects.all(),'posts' : Home.objects.filter(owner = request.user.id)[:5]})

def new_positions(request):
	homes = Home.objects.filter(owner= request.user.id)
	msg = "hello"
	json_data = ""
	if 'json_data' in request.POST:
		json_data = json.loads(request.POST['json_data'])
		for i in range(0,len(json_data)):
			home = homes.get(id=json_data[i]['id'])
			home.position = json_data[i]['newPosition']
			home.save()
			#msg += str(json_data[i]['id']) + " "
	else:
		msg = 'no data'
#	if 'newPositions' in request.POST:
#		new_positions = json.loads(request.POST['newPositions'])
#	else:
#		msg = "no positions"
	return HttpResponse(json.dumps({'msg' : msg }), content_type="application/json")
