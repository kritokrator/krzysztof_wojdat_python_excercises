from __future__ import unicode_literals

#ta linijka juz tu byla
from django.db import models

#to dodalismy, permalink to niezmienny adres danego zasobu sieciowego, takie jakby ID
from django.db.models import permalink
from django.contrib.auth.models import User

# Create your models here.

#to co jest zawarte w tym pliku, to odpowiednik polecen 'create table' z sql

#class HomeManager(models.Manager):
#	def create_home(self, title, slug,body,category,owner):
#		home = self.create(title=title,slug=slug,body=body,category=category,owner=owner)
#		return home



class Home(models.Model):
	#tutaj ustawiamy tytul postu
	title = models.CharField(max_length=100, unique=True)
	#tutaj ustawiamy skrotowa nazwe postu, sluzaca do tworzenia 'ladnego adresu'
	slug = models.SlugField()
	body = models.TextField()
	"""ma byc indexowane przez baze danych, drugi argument mowi,
	 ze jesli nie wstawiamy konkretnej wartosci to ma byc pobrana obecna data automatycznie i wstawiona jako wartosc domyslna"""
	posted =  models.DateTimeField(db_index=True,auto_now_add=True)
	category = models.ForeignKey('home.category')
	owner = models.IntegerField()
	position = models.IntegerField()

	#metoda do zwracania tytulu w wersji unicode
	def __unicode__(self):
		return '%s' % self.title

	""" 'permalink' to adnotacja ktora sluzy do opisania metody ktora bedzie zwracac absolutna sciezke do danego zasobu/postu
	potrzebna jest adnotacja, gdyz ta funkcja jest juz w module permalink importowanym przez nas na gorze pliku,
	tutaj my ja nadpisujemy, (override) czyli zmieniamy jej dzialanie"""

	@permalink
	def get_absolute_url(self):
		return ('view_blog_post', None,{'slug' : self.slug })

class Category(models.Model):
	title = models.CharField(max_length = 100, db_index=True)
	slug = models.SlugField(max_length = 100, db_index=True)
  #metoda do zwracania tytulu w wersji unicode
	def __unicode__(self):
		return '%s' % self.title
	""" 'permalink' to adnotacja ktora sluzy do opisania metody ktora bedzie zwracac absolutna sciezke do danego zasobu/postu
	potrzebna jest adnotacja, gdyz ta funkcja jest juz w module permalink importowanym przez nas na gorze pliku,
	 tutaj my ja nadpisujemy, (override) czyli zmieniamy jej dzialanie"""

	@permalink
	def get_absolute_url(self):
		return ('view_blog_category',None,{'slug' : self.slug})
	
	
