function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function(){
	$('#olympic').click(function(){
		var tmp = $(this).data('img');
		var old_src = $(this).attr('src');
		$(this).data('img',old_src);
		$(this).attr('src',tmp);
	});
    
    $('.main-shapeshift').shapeshift({selector:'.post-shapeshift'});
    var containers = $(".main-shapeshift");
    
   containers.on("ss-drop-complete",function(e){
       var newPositions = [];
       var csrftoken = getCookie('csrftoken');
        containers.children('.post-shapeshift').each(function(){
                var newIndex = $(this).index();
                var id = 0;
                $(this).children('.position').each(function(){
                    id = $(this).html();
                   //$(this).html(newIndex);
                });
            newPositions.push({'id' : id, 'newPosition' : newIndex});
            
       });
		newPositions = JSON.stringify(newPositions);
		var post_data = {'json_data' : newPositions};
		alert(newPositions);
		 var request = $.ajax({
                method:"POST",
                headers: {
                    'X-CSRFToken' : csrftoken,
                },
                url:'/blog/new-positions',
                data: post_data,
            });
            
            request.done(function(msg){
                alert("message delivered " + msg.msg);
            });
            request.fail(function(jqXHR, textStatus){
                alert("request failed: " + textStatus);
            });
   });
});
