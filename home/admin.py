from django.contrib import admin
from home.models import Home, Category
# Register your models here.

"""klasy ktore odpowiadaja za wyswietlanie pol na stronie administratora"""
class HomeAdmin(admin.ModelAdmin):
	exclude = ['posted']
#to pole okresla co powinno byc automatycznie stworzone, i za pomoca jakich innych pol
	prepopulated_fields = {'slug' : ('title',)}

class CategoryAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug' : ('title',)}


#te dwa sformulowania rejestruja to co napisalismy w tym pliku i w pliku models.py we frameworku django, mowia ze tego nalezy uzywac
admin.site.register(Home,HomeAdmin)
admin.site.register(Category,CategoryAdmin)
